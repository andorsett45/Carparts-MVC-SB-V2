package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.service.CarpartsService;

@Controller
public class CarpartsController {

    @Autowired
    CarpartsService carpartsService;

    @GetMapping(value = { "/carparts/partnumber" })
    public String getPartnumber(Model model, String partnumber) throws Exception {
        if (partnumber != null && partnumber.length() > 0) {
            Carpart carpart = carpartsService.getCarpartByPartNumber(partnumber);
            model.addAttribute("carpart", carpart);
        }

        model.addAttribute("active", "add");
        return "partnumber";
    }

    @GetMapping(value = { "/carparts/electrical/{partNumber}" })
    public String getElectrical(@PathVariable String partNumber, Model model) throws Exception {
        String error = "";
        Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);

        if (carpart == null) {
            error = "Sorry, cannot find part number " + partNumber;
        }

        model.addAttribute("carpart", ((carpart == null) ? new Carpart() : carpart));
        model.addAttribute("active", partNumber);
        model.addAttribute("error", error);
        return "carparts";
    }

    @GetMapping(value = { "/carparts/engine/{partNumber}" })
    public String getEngine(@PathVariable String partNumber, Model model) throws Exception {
        String error = "";
        Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);

        if (carpart == null) {
            error = "Sorry, cannot find part number " + partNumber;
        }

        model.addAttribute("carpart", ((carpart == null) ? new Carpart() : carpart));
        model.addAttribute("active", partNumber);
        model.addAttribute("error", error);
        return "carparts";
    }

    @GetMapping(value = { "/carparts/other/{partNumber}" })
    public String getOther(@PathVariable String partNumber, Model model) throws Exception {
        String error = "";
        Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);

        if (carpart == null) {
            error = "Sorry, cannot find part number " + partNumber;
        }

        model.addAttribute("carpart", ((carpart == null) ? new Carpart() : carpart));
        model.addAttribute("active", partNumber);
        model.addAttribute("error", error);
        return "carparts";
    }

    // @ResponseBody
    @GetMapping(value = { "/carparts/carpartManager" })
    public String getCarpartManager(Model model) {
        List<Carpart> carpartList = carpartsService.getCarparts();
        model.addAttribute("carpartList", carpartList);
        model.addAttribute("active", "manage");
        return "carpartManager";
    }
    
  //Save, Read, Update
    @PostMapping(value = { "carparts/partnumber" })
    public String postPartNumber(Model model, String partnumber, String title,
            String update) {
        
        System.out.println("partnumber=>" + partnumber+ " title=>" + title);
        Carpart carpart = new Carpart();
        carpart.setPartNumber(partnumber);
        carpart.setTitle(title);
        carpart.setLine("");
        carpart.setDescription("");
        boolean saved = carpartsService.saveCarpart(carpart);
       
        String action = ("true".equalsIgnoreCase(update)) ? "Updated" : "Saved";
        
        if (saved) {
            model.addAttribute("messageType", "success");
            model.addAttribute("message", "Part Number " + partnumber + " "+action+" Successfully");    
            model.addAttribute("carpart", saved);
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Part Number " + partnumber + 
                    " Not " + action + " Successfully!");               
        }
        
        getCarpartManager(model);
        //return "partnumber";
        return "redirect:/carparts/carpartManager";
    }
    
    //Delete Data
    @GetMapping(value = { "/carparts/delete/{partnumber}" })
    public String deleteCarpart(@PathVariable String partnumber, Model model) throws Exception {
            
        if (partnumber != null && partnumber.length() > 0) {
            Carpart carpart = carpartsService.getCarpartByPartNumber(partnumber);
            carpartsService.deleteCarpartByPartNumber(carpart);
            model.addAttribute("message", "Car Part with part # = " + partnumber + " Deleted Successfully");
        }
        getCarpartManager(model);
        //return "partnumber";
        return "redirect:/carparts/carpartManager";
    }

    @ResponseBody
    @GetMapping(value = "/carparts/getCarpartsData")
    public List<Carpart> getUserData2(Model model) throws Exception {
        List<Carpart> carpartList = carpartsService.getCarparts();
        return carpartList;
    }

    @GetMapping(value = { "/carparts" })
    public String carparts(Model model, String firstName, String lastName) {
        String fullName = ((firstName != null && firstName.length() > 0) ? firstName : "");
        fullName = ((fullName.length() > 0) ? (firstName + " " + lastName) : "");
        model.addAttribute("fullName", fullName);
        return "carparts";
    }

}

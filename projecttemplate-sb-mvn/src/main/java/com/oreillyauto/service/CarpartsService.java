package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.carparts.Carpart;

public interface CarpartsService {
    public Carpart getCarpartByPartNumber(String partNumber) throws Exception;
	public List<Carpart> getCarparts();
	public boolean saveCarpart(Carpart carpart);
	public void deleteCarpartByPartNumber(Carpart carpart);
}

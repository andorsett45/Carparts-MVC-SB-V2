package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.service.CarpartsService;

@Service("carpartsService")
public class CarpartServiceImpl implements CarpartsService {
    
	@Autowired
	CarpartsRepository carpartsRepo;
	
	@Override
	public boolean saveCarpart(Carpart carpart) {
	    Carpart savedCarpart = carpartsRepo.save(carpart);
        return savedCarpart != null;
	}
	
    @Override
    public Carpart getCarpartByPartNumber(String partNumber) throws Exception {    	
        return carpartsRepo.getCarpart(partNumber);
    }
    
	@Override
	public List<Carpart> getCarparts() {
	    return (List<Carpart>)carpartsRepo.findAll();
	}

	@Override
	public void deleteCarpartByPartNumber(Carpart carpart) {
	    carpartsRepo.delete(carpart);
	}

}

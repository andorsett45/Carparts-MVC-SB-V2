<%@ include file="/WEB-INF/layouts/include.jsp"%>

<h1>Carpart Manager</h1>

<a href="/carparts/partnumber" class="btn btn-primary">Create Part</a>	 

<orly-table idproperty= "partNumber" loaddataoncreate includefilter maxrows="10" tabletitle="Search Results" class="invisible" url="<c:url value='/carparts/getCarpartsData' />">
	<orly-column field="action" label="Action" sorttype="natural">
		<div slot="cell">
	      		<%-- <a href="\${`${pageContext.request.contextPath}/carparts/partnumber?partnumber=\${model.partNumber}`}"></a> 
	      		<a href="\${`${pageContext.request.contextPath}/carparts/delete/\${model.partNumber}`}">\${model.partNumber}</a>--%>
	      <orly-icon name="edit" color="orange" data-id=\${model.partNumber}></orly-icon> 
      	  <orly-icon name="trash-2" color="tomato" data-id=\${model.partNumber}></orly-icon> 
    	</div>
	</orly-column>
	<orly-column field="partNumber" label="Part Number"></orly-column>
	<orly-column field="title" label="Title"></orly-column>
	<orly-column field="line" label="Line"></orly-column>
	<orly-column field="description" label="Description"></orly-column>
</orly-table>

<script>

orly.ready.then(()=> {
let table = orly.q("orly-table");

orly.on(table, "click", (e)=>{
	let partNum = e.target.closest("tr").getAttribute("data-id");
	
	//console.log("button id:" + partNum);
	if(e.target.name == "edit"){
		//window.location.href = "\${`${pageContext.request.contextPath}/carparts/partnumber?partnumber=\${model.partNumber}`}";
		window.location.href = "/carparts/partnumber?partnumber=" + partNum;
	}
	else if (e.target.name == "trash-2"){
		//window.location.href = "\${`${pageContext.request.contextPath}/carparts/delete/\${model.partNumber}`}";
		window.location.href = "/carparts/delete/" + partNum;
	}
});


//orly.qid("e_${model.partNumber}");
}
);
</script>
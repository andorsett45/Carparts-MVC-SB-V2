<%@ include file="/WEB-INF/layouts/include.jsp"%>

<h1>Add New Car Part</h1>
<div class="row">
    <div class="col-sm-12">
	<div>${message}</div>
	<form method="post" action="<c:url value='/carparts/partnumber' />">
	<c:if test="${not empty carpart}">
    	<input type="hidden" id="update" name="update" value="true" />
	</c:if>
	
	    <div class="col-sm-4 form-group">
	        <label for="partnumber">Part #</label>
		<orly-input id="partnumber" name="partnumber" placeholder="Part #"  value="${carpart.partNumber}"></orly-input>
	    </div>
	    <div class="col-sm-4 form-group">
	        <label for="title">Title</label>
		<orly-input id="title" name="title" placeholder="Title" value="${carpart.title}"></orly-input>
	    </div>
	    <div class="col-sm-4 form-group">
	        <button type="submit" class="btn btn-primary">Submit</button>	    
        </div>
	</form>
	<c:if test="${not empty carpart}">
	    <b>Part Number:</b> ${carpart.partNumber}<br/>
	    <b>Title:</b> ${carpart.title}<br/>
	    <a href="<c:url value='/carparts/delete/${carpart.partNumber}'/>" class="btn btn-danger">Delete</a>
	</c:if>

	
</div>
</div>
